import {BaseService} from "./http.service";
import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {RepostModel} from "../models/repost.model";
import {AppService} from "./app.service";

@Injectable({
  providedIn: 'root'
})
export class RepostService {
  constructor(
    private baseService: BaseService,
    private appService: AppService
  ) {
  }

  createRepost(postId: number): Observable<RepostModel> {
    return this.baseService.post<RepostModel>("/reposts", {
      user: {id: this.appService.user?.id},
      post: {id: postId}
    })
  }

  deleteRepost(repostId: number): Observable<number> {
    return this.baseService.delete(`/reposts/${repostId}`);
  }
}
