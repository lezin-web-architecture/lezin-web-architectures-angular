import {BaseService} from "./http.service";
import {Observable} from "rxjs";
import {PostModel} from "../models/post.model";
import {Injectable} from "@angular/core";

@Injectable({
  providedIn: 'root'
})
export class FeedService {
  constructor(private baseService: BaseService) {
  }

  getFeed(): Observable<PostModel[]> {
    return this.baseService.get<PostModel[]>("/feed")
  }

  getFeedByUserId(userId: number): Observable<PostModel[]> {
    return this.baseService.get<PostModel[]>(`/feed/${userId}`)
  }

  getFeedOfRepliesByUserId(userId: number): Observable<PostModel[]> {
    return this.baseService.get<PostModel[]>(`/feed/replies/${userId}`)
  }

  getFeedOfLikedPostsByUserId(userId: number): Observable<PostModel[]> {
    return this.baseService.get<PostModel[]>(`/feed/likes/${userId}`)
  }
}
