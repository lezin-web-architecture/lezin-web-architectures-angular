import {BaseService} from "./http.service";
import {Injectable} from "@angular/core";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class LikeService {

  constructor(private baseService: BaseService) {
  }

  createLike(userId: number, postId: number): Observable<number> {
    return this.baseService.post<number>('/likes', {
      user: {
        id: userId,
      },
      post: {
        id: postId
      }
    })
  }

  deleteLike(likeId: number): Observable<number> {
    return this.baseService.delete(`/likes/${likeId}`)
  }
}
