import {BaseService} from "./http.service";
import {Observable} from "rxjs";
import {Injectable} from "@angular/core";
import {AppService} from "./app.service";
import {UserModel} from "../models/user.model";

@Injectable({
  providedIn: 'root'
})
export class FollowerService {
  constructor(private baseService: BaseService, private appService: AppService) {
  }

  createFollower(followOnId: number): Observable<any> {
    return this.baseService.post<any>("/follow",
      {
        user: {
          id: this.appService.user?.id
        },
        followsOn: {
          id: followOnId
        }
      }
    )
  }

  deleteFollower(unFollowFromId: number) {
    return this.baseService.post<any>("/follow/unfollow",
      {
        user: {
          id: this.appService.user?.id
        },
        followsOn: {
          id: unFollowFromId
        }
      }
    )
  }

  getFollowingUsers(userId: number): Observable<UserModel[]> {
    return this.baseService.get<UserModel[]>("/follow/following-users/" + userId)
  }

  getFollowerUsers(userId: number): Observable<UserModel[]> {
    return this.baseService.get<UserModel[]>("/follow/follower-users/" + userId)
  }
}
