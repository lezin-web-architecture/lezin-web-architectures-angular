import {Injectable} from '@angular/core';
import {BaseService} from "./http.service";
import {PostModel} from "../models/post.model";
import {Observable} from "rxjs";
import {AppService} from "./app.service";

@Injectable({
  providedIn: 'root'
})
export class PostService {

  constructor(private baseService: BaseService, private appService: AppService) {
  }

  getAllPosts(): Observable<PostModel[]> {
    return this.baseService.get<PostModel[]>('/posts');
  }

  createPost(data: { text: string }, parentPostId: number | undefined | null): Observable<PostModel> {
    const params: any = {
      ...data,
      user: {id: this.appService.user?.id},
      ...(parentPostId != null ? {parentPost: {id: parentPostId}} : {})
    }
    return this.baseService.post("/posts/create", {...data, ...params})
  }

  deletePost(postId: number) {
    return this.baseService.delete(`/posts/${postId}`)
  }

  getPostById(postId: number): Observable<PostModel> {
    return this.baseService.get<PostModel>(`/posts/${postId}`)
  }
}
