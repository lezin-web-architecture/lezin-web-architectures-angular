import {BaseService} from "./http.service";
import {Observable} from "rxjs";
import {UserModel} from "../models/user.model";
import {Injectable} from "@angular/core";

@Injectable({
  providedIn: 'root'
})
export class UserService {
  constructor(private baseService: BaseService) {
  }

  getUserById(userId: number): Observable<UserModel> {
    return this.baseService.get(`/users/${userId}`)
  }
}
