export interface ILoginParams {
  login: string;
  password: string;
}

export interface ISignupParams extends ILoginParams{
  name: string;
  repeatedPassword: string;
}
