import {UserModel} from "./user.model";
import {BaseModel} from "./base.model";

export class PostModel extends BaseModel {
  id: number;
  createdAt: string;
  text: string;
  user: UserModel;
  replies: PostModel[];
  likes: number[];
  reposts: number[];
  repostedUser: UserModel | null;
  parentPostId: number | null;

  constructor(id: number, createdAt: string, text: string, user: UserModel, replies: PostModel[],
              likes: number[], repostedUser: UserModel, reposts: number[], parentPostId: number | null) {
    super();
    this.id = id;
    this.createdAt = createdAt;
    this.text = text;
    this.user = user;
    this.replies = replies;
    this.likes = likes;
    this.repostedUser = repostedUser;
    this.reposts = reposts;
    this.parentPostId = parentPostId;
  }
}

export enum PostTypeEnum {
  POST = 'POST',
  REPLY = 'REPLY'
}
