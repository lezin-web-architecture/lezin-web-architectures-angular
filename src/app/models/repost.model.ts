import {UserModel} from "./user.model";
import {BaseModel} from "./base.model";
import {PostModel} from "./post.model";

export class RepostModel extends BaseModel {
  id: number;
  createdAt: string;
  user: UserModel;
  post: PostModel;

  constructor(id: number, createdAt: string, text: string, user: UserModel, post: PostModel) {
    super();
    this.id = id;
    this.createdAt = createdAt;
    this.user = user;
    this.post = post;
  }
}
