export class UserModel {
  id: number;
  login: string;
  name: string;
  description?: string;
  likes: number[];
  reposts: number[];
  followers: number[] = [];
  followings: number[] = [];

  constructor(id: number, login: string, name: string, likes: number[],
              reposts: number[], description: string, followers: number[], followings: number[]) {
    this.id = id;
    this.login = login;
    this.name = name;
    this.likes = likes;
    this.reposts = reposts;
    this.description = description;
    this.followers = followers;
    this.followings = followings;
  }
}
