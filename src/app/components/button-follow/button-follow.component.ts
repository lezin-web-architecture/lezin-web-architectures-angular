import {Component, EventEmitter, Input, Output} from '@angular/core';
import {AppService} from "../../services/app.service";

@Component({
  selector: 'app-button-follow',
  templateUrl: './button-follow.component.html',
  styleUrls: ['./button-follow.component.scss']
})
export class ButtonFollowComponent {
  @Input() compareUserId!: number;
  @Output() clickOnFollowEvent = new EventEmitter<boolean>();

  constructor(private appService: AppService) {
  }

  get isCurrentUserProfile(): boolean {
    return this.appService.user?.id === this.compareUserId;
  }

  get isCurrentUserFollowedProfile(): boolean {
    return !!this.appService.user?.followings.includes(this.compareUserId);
  }

  get followButtonString(): string {
    return this.isCurrentUserFollowedProfile ? 'Отписаться' : 'Подписаться';
  }

  clickOnFollow(e: MouseEvent) {
    e.stopPropagation();
    this.clickOnFollowEvent.emit(this.isCurrentUserFollowedProfile);
  }
}
