import {Component, EventEmitter, Input, Output} from '@angular/core';
import {PostService} from "../../services/post.service";
import {PostModel, PostTypeEnum} from "../../models/post.model";
import {ActivatedRoute} from "@angular/router";


@Component({
  selector: 'app-create-post',
  templateUrl: './create-post.component.html',
  styleUrls: ['./create-post.component.scss']
})
export class CreatePostComponent {
  @Output() createPostEvent = new EventEmitter<void>();
  @Input() postType: PostTypeEnum = PostTypeEnum.POST;
  @Input() post?: PostModel;

  constructor(private postService: PostService, private route: ActivatedRoute) {
    this.route.queryParams.subscribe(params => {
      this.parentPostId = params['parentPostId'];
    });
  }

  data: { text: string } = {text: ''};
  isFocused: boolean = false;
  private parentPostId!: number | null;

  get buttonPlaceholder(): string {
    return this.postType === PostTypeEnum.POST ? 'Опубликовать пост' : 'Ответить'
  }

  get textareaPlaceholder(): string {
    return this.postType === PostTypeEnum.POST ? 'Поделитесь своим настроением!' : `Ответить @${this.post?.user.login}`
  }

  createPost() {
    this.postService.createPost(this.data, this.parentPostId ?? this.post?.parentPostId).subscribe(() => {
      this.createPostEvent.emit();
      this.data.text = '';
    });
  }
}
