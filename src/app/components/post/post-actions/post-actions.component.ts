import {Component, Input} from '@angular/core';
import {PostModel} from "../../../models/post.model";
import {LikeService} from "../../../services/like.service";
import {AppService} from "../../../services/app.service";
import {RepostService} from "../../../services/repost.service";
import {CURRENT_USER_ID} from "../../../../utils/constants";
import {MatSnackBar} from "@angular/material/snack-bar";

@Component({
  selector: 'app-post-actions',
  templateUrl: './post-actions.component.html',
  styleUrls: ['./post-actions.component.scss']
})
export class PostActionsComponent {
  @Input() post!: PostModel

  constructor(private likeService: LikeService,
              private appService: AppService,
              private repostService: RepostService,
              private snackBar: MatSnackBar
  ) {
  }

  get postIsLiked(): boolean {
    return !!this.appService.user?.likes.some(userLike => this.post.likes.includes(userLike));
  }

  get postIsReposted(): boolean {
    return !!this.appService.user?.reposts.some(repostId => this.post.reposts.includes(repostId));
  }

  clickOnLike(e: MouseEvent) {
    e.stopPropagation();
    if (this.postIsLiked) {
      this.deleteLike();
    } else {
      this.createLike();
    }
  }

  clickOnRepost(e: MouseEvent) {
    e.stopPropagation();
    if (this.postIsReposted) {
      this.deleteRepost();
    } else {
      if (this.appService.user?.id === this.post.user.id) {
        this.snackBar.open("Невозможно сделать репост своего поста")
        return;
      }
      this.createRepost();
    }
  }

  private createLike() {
    this.likeService.createLike(this.appService.user?.id as number, this.post.id).subscribe((likeId: number | null) => {
      if (likeId !== null) {
        // todo: наверное стоит обновить всю ленту
        this.post.likes.push(likeId);
        this.appService.user?.likes.push(likeId);
      }
    })
  }

  private deleteLike() {
    if (!this.appService.user?.likes || !this.post?.likes) return;
    const likeId: number = this.appService.user.likes.find(userLike => this.post.likes.includes(userLike)) as number;
    this.likeService.deleteLike(likeId).subscribe((likeId: number | null) => {
      // todo: наверное стоит обновить всю ленту
      this.post.likes = this.post.likes.filter(l => l !== likeId);
      this.appService.user!.likes = this.appService.user!.likes.filter(l => l !== likeId);
    });
  }

  private createRepost() {
    this.repostService.createRepost(this.post.id).subscribe(repost => {
      this.post.reposts.push(repost.id);
      this.appService.user?.reposts.push(repost.id);
    })
  }

  private deleteRepost() {
    if (!this.appService.user?.reposts || !this.post.reposts) return;
    const repostId = this.appService.user?.reposts.find(repostId => this.post.reposts.includes(repostId)) as number;
    this.repostService.deleteRepost(repostId).subscribe(id => {
      // todo: eventBus;
      this.appService.user!.reposts = this.appService.user!.reposts.filter(r => r !== id);
      this.post.reposts = this.post.reposts.filter(r => r !== id);
      console.log('удалил пост')
    });
  }
}
