import {Component, EventEmitter, Input, Output} from '@angular/core';
import {PostModel} from "../../models/post.model";
import {PostService} from "../../services/post.service";
import {Router} from "@angular/router";
import {AppService} from "../../services/app.service";

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss']
})
export class PostComponent {
  @Output() deletePostEvent = new EventEmitter<void>();
  @Input() post!: PostModel;
  @Input() showReplies?: boolean = false;

  constructor(private postService: PostService,
              private appService: AppService,
              private router: Router) {
  }

  get repostedByString(): string {
    if (this.post.repostedUser?.id === this.appService.user?.id) {
      return "Вы сделали репост";
    } else {
      return `${this.post.repostedUser?.name} сделал(а) репост`
    }
  }

  deletePost(postId: number, e: any) {
    e.stopPropagation();
    this.postService.deletePost(postId).subscribe(() => {
      this.deletePostEvent.emit();
    });
  }

  goToDetailPost(id: number) {
    this.router.navigate(['/posts', id], {queryParams: {parentPostId: this.post.id}});
  }

  goToUser(e: MouseEvent) {
    e.stopPropagation();
    this.router.navigate(['/profile', this.post.user.id]);
  }
}
