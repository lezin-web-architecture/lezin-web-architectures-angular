import {Component} from '@angular/core';
import {PostService} from "../../../services/post.service";
import {ActivatedRoute, Router} from "@angular/router";
import {PostModel} from "../../../models/post.model";
import {PostTypeEnum} from "../../../models/post.model";

@Component({
  selector: 'app-post-detail',
  templateUrl: './post-detail.component.html',
  styleUrls: ['./post-detail.component.scss']
})
export class PostDetailComponent {
  post!: PostModel;
  loading: boolean = true;
  PostTypeEnum = PostTypeEnum;

  constructor(private postService: PostService, private route: ActivatedRoute, private router: Router) {
    this.route.params.subscribe(params => {
      const postId = +params['id'];
      this.getPostById(postId);
    });
  }

  getPostById(id: number) {
    this.postService.getPostById(id).subscribe(post => {
      this.post = post;
      this.loading = false;
    })
  }

  goToUser(e: MouseEvent) {
    e.stopPropagation();
    this.router.navigate(['/profile', this.post.user.id]);
  }
}
