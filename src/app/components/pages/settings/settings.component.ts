import { Component } from '@angular/core';
import {AuthService} from "../../../services/auth.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent {
  constructor(
    private authService: AuthService,
    private router: Router
  ) {
  }
  logout() {
    this.authService.logout().subscribe({
      complete: () => {
        window.location.replace("/");
      },
    });
  }
}
