import {Component, OnInit} from '@angular/core';
import {UserModel} from "../../../models/user.model";
import {FollowerService} from "../../../services/follower.service";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-followers',
  templateUrl: './follow-list.component.html',
  styleUrls: ['./follow-list.component.scss']
})
export class FollowListComponent implements OnInit {
  users!: UserModel[];
  profileId!: number;

  constructor(private followService: FollowerService, private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.calculateProfileId();
    this.getData();
  }

  calculateProfileId() {
    const urlSegments = this.route.snapshot.url;
    const profileUrlIndex = urlSegments.findIndex(segment => segment.path = 'profile')
    this.profileId = +urlSegments[profileUrlIndex + 1].path;
  }

  getAllFollowingUsers() {
    this.followService.getFollowingUsers(this.profileId).subscribe(users => {
      this.users = users;
    })
  }

  getAllFollowerUsers() {
    this.followService.getFollowerUsers(this.profileId).subscribe(users => {
      this.users = users;
    })
  }

  getData() {
    if (this.route.snapshot.url.find((segment) => segment.path === 'followers')) {
      this.getAllFollowerUsers()
    } else {
      this.getAllFollowingUsers();
    }
  }
}
