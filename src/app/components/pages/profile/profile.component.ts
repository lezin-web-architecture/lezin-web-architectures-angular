import {Component, OnInit} from '@angular/core';
import {UserModel} from "../../../models/user.model";
import {UserService} from "../../../services/user.service";
import {FeedTypeEnum} from "../../../../utils/constants";
import {ActivatedRoute} from "@angular/router";
import {AppService} from "../../../services/app.service";
import {FollowerService} from "../../../services/follower.service";

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  user: UserModel = {} as UserModel; // = {} as UserModel пока нет лоадинга
  tabs: any = [
    {type: FeedTypeEnum.FOR_USER, title: 'Посты', isActive: true},
    {type: FeedTypeEnum.FOR_USER_BY_REPLIES, title: 'Ответы', isActive: false},
    {type: FeedTypeEnum.FOR_USER_BY_LIKES, title: 'Нравится', isActive: false}
  ];
  paramsForRequest: any = {
    userId: null
  }

  constructor(private userService: UserService,
              private route: ActivatedRoute,
              private followerService: FollowerService,
              private appService: AppService) {
    this.route.params.subscribe(params => {
      this.paramsForRequest = {
        userId: +params['id']
      };
    });
  }

  ngOnInit() {
    this.getUserById()
  }

  getUserById() {
    this.userService.getUserById(this.paramsForRequest.userId).subscribe(user => this.user = user);
  }

  get currentFeedType(): FeedTypeEnum {
    return this.tabs.find((tab: any) => tab.isActive).type
  }

  get isCurrentUserProfile(): boolean {
    return this.appService.user?.id === this.paramsForRequest.userId;
  }

  clickOnFollow(value: boolean) {
    if (value) {
      this.unFollow();
    } else {
      this.follow();
    }
  }

  follow() {
    this.followerService.createFollower(this.paramsForRequest.userId).subscribe(follower => {
      this.appService.user?.followings.push(this.paramsForRequest.userId);
      this.getUserById();
    });
  }

  unFollow() {
    this.followerService.deleteFollower(this.paramsForRequest.userId).subscribe(follower => {
      if (this.appService.user) {
        this.appService.user.followings = this.appService.user.followings.filter(id => id !== this.paramsForRequest.userId);
      }
      this.getUserById();
    });
  }


  setTab(tab: any) {
    this.tabs.forEach((t: any) => t.isActive = false);
    tab.isActive = true;
  }
}
