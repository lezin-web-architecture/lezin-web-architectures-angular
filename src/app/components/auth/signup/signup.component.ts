import {Component} from '@angular/core';
import {ILoginParams, ISignupParams} from "../../../models/auth/auth-params.model";
import {AuthService} from "../../../services/auth.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['../../../../styles/components/auth.styles.scss']
})
export class SignupComponent {
  model: ISignupParams = {login: '', password: '', name: '', repeatedPassword: ''};
  serverError: string | null = null;

  constructor(
    private authService: AuthService,
    private router: Router
  ) {
  }

  signup() {
    this.serverError = null;
    this.authService.signUp(this.model).subscribe({
      complete: () => {
        this.router.navigate(['auth/login']);
      },
      error: (err) => {
        this.serverError = err
      }
    });
  }
}
