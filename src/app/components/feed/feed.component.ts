import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {PostModel} from "../../models/post.model";
import {FeedService} from "../../services/feed.service";
import {FeedTypeEnum} from "../../../utils/constants";

@Component({
  selector: 'app-feed',
  templateUrl: './feed.component.html',
  styleUrls: ['./feed.component.scss']
})
export class FeedComponent implements OnInit, OnChanges {
  @Input() showCreatePost?: boolean = true;
  @Input() feedType: FeedTypeEnum = FeedTypeEnum.ALL;
  @Input() paramsForRequest?: any = {};
  posts: PostModel[] = [];

  constructor(private feedService: FeedService) {
  }

  ngOnInit(): void {
    this.getPosts();
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.getPosts();
  }

  private getFeed() {
    this.feedService.getFeed().subscribe(posts => {
      this.posts = posts;
    })
  }

  private getAllPostsByUserId() {
    this.feedService.getFeedByUserId(this.paramsForRequest.userId).subscribe(posts => {
      this.posts = posts;
    })
  }

  private getFeedOfRepliesByUserId() {
    this.feedService.getFeedOfRepliesByUserId(this.paramsForRequest.userId).subscribe(posts => {
      this.posts = posts
    })
  }

  private getFeedOfLikedPostsByUserId() {
    this.feedService.getFeedOfLikedPostsByUserId(this.paramsForRequest.userId).subscribe(posts => {
      this.posts = posts;
    })
  }


  public getPosts() {
    switch (this.feedType) {
      case FeedTypeEnum.ALL: {
        this.getFeed();
        break;
      }
      case FeedTypeEnum.FOR_USER: {
        this.getAllPostsByUserId();
        break;
      }
      case FeedTypeEnum.FOR_USER_BY_LIKES: {
        this.getFeedOfLikedPostsByUserId()
        break;
      }
      case FeedTypeEnum.FOR_USER_BY_REPLIES: {
        this.getFeedOfRepliesByUserId();
        break;
      }
    }
  }
}
