import { Component } from '@angular/core';
import {Router} from "@angular/router";
import {AppService} from "../../services/app.service";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {

  constructor(private router: Router, public appService: AppService) {
  }

  navigateTo(url: string) {
    this.router.navigate([url])
  }
}
