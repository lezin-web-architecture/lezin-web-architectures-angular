import {Component} from '@angular/core';
import {UserModel} from "../../../models/user.model";
import {AppService} from "../../../services/app.service";

@Component({
  selector: 'app-user-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.scss']
})
export class UserInfoComponent {
  user!: UserModel;

  constructor(private appService: AppService) {
    this.user = this.appService.user as UserModel;
  }
}
