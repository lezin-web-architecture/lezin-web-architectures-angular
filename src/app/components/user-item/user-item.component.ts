import {Component, EventEmitter, Input, Output} from '@angular/core';
import {UserModel} from "../../models/user.model";
import {Router} from "@angular/router";
import {AppService} from "../../services/app.service";
import {FollowerService} from "../../services/follower.service";

@Component({
  selector: 'app-user-item',
  templateUrl: './user-item.component.html',
  styleUrls: ['./user-item.component.scss']
})
export class UserItemComponent {
  @Input() user!: UserModel;
  @Output() userChange = new EventEmitter();

  constructor(private router: Router, private appService: AppService, private followService: FollowerService) {
  }

  get isCurrentUserFollowedProfile(): boolean {
    return !!this.appService.user?.followings.includes(this.user && this.user.id);
  }

  goToUser() {
    this.router.navigate(['/profile', this.user.id])
  }

  clickOnFollow() {
    if (this.isCurrentUserFollowedProfile) {
      this.unFollow();
    } else {
      this.follow();
    }
  }

  follow() {
    this.followService.createFollower(this.user.id);
  }

  unFollow() {
    this.followService.deleteFollower(this.user.id);
  }
}
