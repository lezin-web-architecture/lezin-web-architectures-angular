import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FeedComponent} from './components/feed/feed.component';
import {PostComponent} from './components/post/post.component';
import {HeaderComponent} from "./components/header/header.component";
import {MatIconModule} from "@angular/material/icon";
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {CreatePostComponent} from './components/create-post/create-post.component';
import {FormsModule} from "@angular/forms";
import {UserInfoComponent} from './components/header/user-info/user-info.component';
import {MatProgressSpinnerModule} from "@angular/material/progress-spinner";
import {LoginComponent} from './components/auth/login/login.component';
import {PostDetailComponent} from './components/post/post-detail/post-detail.component';
import {PostActionsComponent} from './components/post/post-actions/post-actions.component';
import {ProfileComponent} from './components/pages/profile/profile.component';
import {MAT_SNACK_BAR_DEFAULT_OPTIONS, MatSnackBarModule} from "@angular/material/snack-bar";
import {MainComponent} from './components/pages/main/main.component';
import {FollowListComponent} from './components/pages/followers/follow-list.component';
import {UserItemComponent} from './components/user-item/user-item.component';
import {UserPhotoComponent} from './components/user-photo/user-photo.component';
import {ButtonFollowComponent} from './components/button-follow/button-follow.component';
import {EmptyListComponent} from "./components/common/empty-list/empty-list.component";
import {AuthInterceptor} from "./services/auth-interceptor.service";
import { SettingsComponent } from './components/pages/settings/settings.component';
import { SignupComponent } from './components/auth/signup/signup.component';

@NgModule({
  declarations: [
    AppComponent,
    FeedComponent,
    PostComponent,
    HeaderComponent,
    CreatePostComponent,
    UserInfoComponent,
    LoginComponent,
    PostDetailComponent,
    PostActionsComponent,
    ProfileComponent,
    MainComponent,
    FollowListComponent,
    UserItemComponent,
    UserPhotoComponent,
    ButtonFollowComponent,
    EmptyListComponent,
    EmptyListComponent,
    SettingsComponent,
    SignupComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatIconModule,
    HttpClientModule,
    FormsModule,
    MatProgressSpinnerModule,
    MatSnackBarModule
  ],
  providers: [
    {provide: MAT_SNACK_BAR_DEFAULT_OPTIONS, useValue: {duration: 2500}},
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true,
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
