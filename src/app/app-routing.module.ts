import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from "./components/auth/login/login.component";
import {PostDetailComponent} from "./components/post/post-detail/post-detail.component";
import {ProfileComponent} from "./components/pages/profile/profile.component";
import {MainComponent} from "./components/pages/main/main.component";
import {FollowListComponent} from "./components/pages/followers/follow-list.component";
import {SettingsComponent} from "./components/pages/settings/settings.component";
import {SignupComponent} from "./components/auth/signup/signup.component";

const routes: Routes = [
  {path: '', component: MainComponent},
  {path: 'settings', component: SettingsComponent},
  {path: 'auth/login', component: LoginComponent},
  {path: 'auth/signup', component: SignupComponent},
  {path: 'posts/:id', component: PostDetailComponent},
  {path: 'profile/:id', component: ProfileComponent},
  {path: 'profile/:id/followers', component: FollowListComponent},
  {path: 'profile/:id/followings', component: FollowListComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
